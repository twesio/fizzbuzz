package fizzbuzz;

import java.util.ArrayList;
import java.util.List;

public class App01 {

    public static void main(String[] args) {
        for (int i = 1; i <= 100; i++) {
            List<String> strings = new ArrayList<>();

            if (i % 3 == 0) {
                strings.add("Fizz");
            }

            if (i % 5 == 0) {
                strings.add("Buzz");
            }

            if (strings.isEmpty()) {
                strings.add(Integer.toString(i));
            }

            System.out.println(String.join("", strings));
        }
    }

}
