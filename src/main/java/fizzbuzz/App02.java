package fizzbuzz;

import java.util.ArrayList;
import java.util.List;

public class App02 {

    public static void main(String[] args) {
        for (int i = 1; i <= 100; i++) {
            List<String> strings = new ArrayList<>();

            String asString = Integer.toString(i);

            if (i % 3 == 0 || asString.contains("3")) {
                strings.add("Fizz");
            }

            if (i % 5 == 0 || asString.contains("5")) {
                strings.add("Buzz");
            }

            if (strings.isEmpty()) {
                strings.add(asString);
            }

            System.out.println(String.join("", strings));
        }
    }

}
