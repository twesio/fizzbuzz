package fizzbuzz;

import java.util.ArrayList;
import java.util.List;

public class App03 {

    public static void main(String[] args) {
        App03 app03 = new App03();

        app03.loop(100);
    }

    public void loop(int iterations) {
        for (int i = 1; i <= iterations; i++) {
            String x = get(i);

            System.out.println(x);;
        }
    }

    public String get(int i) {
        List<String> strings = new ArrayList<>();

        String asString = Integer.toString(i);

        if (i % 3 == 0 || asString.contains("3")) {
            strings.add("Fizz");
        }

        if (i % 5 == 0 || asString.contains("5")) {
            strings.add("Buzz");
        }

        if (strings.isEmpty()) {
            strings.add(asString);
        }

        return String.join("", strings);
    }

}
