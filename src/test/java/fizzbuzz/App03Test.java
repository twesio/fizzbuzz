package fizzbuzz;

import org.junit.Test;

import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.Assert.assertThat;

/**
 * Examples are based on
 *  https://ccd-school.de/coding-dojo/function-katas/fizzbuzz/
 */
public class App03Test {

    private App03 createInstance() {
        return new App03();
    }

    @Test
    public void testExamples() {
        App03 instance = createInstance();

        assertThat(instance.get(1), is("1"));
        assertThat(instance.get(2), is("2"));
        assertThat(instance.get(3), is("Fizz"));
        assertThat(instance.get(5), is("Buzz"));
        assertThat(instance.get(6), is("Fizz"));
        assertThat(instance.get(7), is("7"));

        assertThat(instance.get(14), is("14"));
        assertThat(instance.get(15), is("FizzBuzz"));
        assertThat(instance.get(16), is("16"));
    }

    @Test
    public void testDivisibleByThreeResultsInFizz() {
        App03 instance = createInstance();

        for(int i = 0; i < 1000; i++) {
            assertThat(instance.get(i * 3), startsWith("Fizz"));
        }
    }

    @Test
    public void testDivisibleByFiveResultsInBuzz() {
        App03 instance = createInstance();

        for(int i = 0; i < 1000; i++) {
            assertThat(instance.get(i * 5), endsWith("Buzz"));
        }
    }

    @Test
    public void testDivisibleByThreeAndFiveResultsInFizzBuzz() {
        App03 instance = createInstance();

        for(int i = 0; i < 1000; i++) {
            assertThat(instance.get(i * 3 * 5), is("FizzBuzz"));
        }
    }

}
