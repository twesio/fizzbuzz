# Fizz Buzz – <small>Function Kata "FizzBuzz"</small>

Based on https://ccd-school.de/coding-dojo/function-katas/fizzbuzz/

| Implementation | Unit Test | Notes |
| ---            | ---       | ---   |
| [App01](src/main/java/fizzbuzz/App01.java) | _not testable_ | Basic implementation |
| [App02](src/main/java/fizzbuzz/App02.java) | _not testable_ | Variation: Also prints "Fizz" or "Buzz" if the number contains the character "3" or "5" respectively. |
| [App03](src/main/java/fizzbuzz/App03.java) | [App03Test](src/test/java/fizzbuzz/App03Test.java) | Testable implementation of App02 |

---

Greetings! :)
